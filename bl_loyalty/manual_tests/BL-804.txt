Test Cases for BL-804

Loaded Language

  	Check loaded language - should be set according to web-browser setting

  			1. Set schema available language:
  					- no-norsk
  					- pl-polski
  					- en-english
  					- nb-norsk b
  			2. Set schema default language: no-norwegian


    TC001
    	Test scenario: Check language loaded in webforms
    		Steps:
    			3. Set web-browser language to one of available language
    			4. Go to https://webforms.dev.bstcm.no/newschema2
    			5. Input telephone number
    			6. Check propertiest translation

    		Expected Result:
    			Display translation for choosen language.

    TC002
    	Test scenario: Check language loaded in webforms
    		Steps:
    			3. Set web-browser language to one of non-available language
    			4. Go to https://webforms.dev.bstcm.no/newschema2
    			5. Input telephone number
    			6. Check propertiest translation

        Test Data:


    		Expected Result:
    			Display translation for default language.


Loaded Property Translation

      Check Property name according to translation
          1. Set schema available language:
              - no-norsk
              - pl-polski
              - en-english
              - nb-norsk b
          2. Set schema default language: no-norwegian

    TC001
        Steps:
          3. Set web-browser language to one of non-available language
          4. Go to https://webforms.dev.bstcm.no/newschema2
          5. Input telephone number
          6. Check propertiest translation according to settings in Member
          Properties

        Expected result:
          Properties names dispaly due to translation in default language.