Test casese for BL-714

Loaded Language

	Check loaded language - should be set according to web-browser setting

			1. Set schema available language:
					- no-norsk
					- pl-polski
					- en-english
					- nb-norsk b
			2. Set schema default language: no-norwegian


TC001
	Test scenario: Check language loaded in webforms
		Steps:
			3. Set web-browser language to one of available language
			4. Go to https://webforms.dev.bstcm.no/newschema2
			5. Input telephone number
			6. Check propertiest translation

		Expected Result:
			Display translation for choosen language.

TC002
	Test scenario: Check language loaded in webforms
		Steps:
			3. Set web-browser language to one of non-available language
			4. Go to https://webforms.dev.bstcm.no/newschema2
			5. Input telephone number
			6. Check propertiest translation

		Expected Result:
			Display translation for default language.
