Test Cases for BL-747

    Change Schema to v2 without translations.

        TC001:
            Set schema to v2 in schema editor.

                1. Choose customer with schema v1.
                2. Go to "Loyalty Club settings"
                3. Go to "Schema editor"
                4. Add schema version to editor.
                        "version": "v2",
                5. Go to "Member Properties"

            Expected result:
                No display error message for lack of translation.

        TC002:
            Set schema to non existing version

                1. Choose customer with schema v1.
                2. Go to "Loyalty Club settings"
                3. Go to "Schema editor"
                4. Add schema version to editor.
                        "version": "v34",
                5. Go to "Member Properties"

            Expected result:
                Set schema to default schema
