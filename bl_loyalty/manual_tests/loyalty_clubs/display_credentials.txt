Display API Credentials

TC001
	Test Scenario: Positive API Credentials display
		Steps:
			1. Login as user from Test data
			2. Go to Loyalty Clubs
			3. Display API Credentials
		
	Test Data:
		- l/p: platform.adm@boost.no / 12341234
		
	Expected result:
		Window display API Credentials. 
			- Loyalty Club Slug
			- Customer Public Token
			- Customer Private Token
			- MP Customer name
			- MP Customer token

TC002
	Test Scenario: Negative API Credentials display
		Steps:
			1. Login as Test data
			2. Go to Loyalty Clubs
			3. Display API Credentials

	Test data: 
		- l/p: mp.qa+bl.global.admin@boostcom.no / qatest1234 - Admin for group of chains
		- l/p: mp.qa+bl.norway.admin@boostcom.no / qatest1234 - Admin for a single chain
		- l/p: mp.qa+bl.global.manager@boostcom.no /qatest1234 - Manager for group of chains
		- l/p: mp.qa+bl.manager@boostcom.no / qatest1234 - Chain Manager
		- l/p: mp.qa+bl.mebers.manager@boostcom.no / qatest1234 - Chain Members Manager
		- l/p: mp.qa+bl.mall.mebers.manager@boostcom.no / qatest1234 - Mall Members Manager
		- l/p: mp.qa+bl.user@boostcom.no / qatest1234 - Mall user
		
	Expected result:
		Display API Credentials option invisible for that user. 