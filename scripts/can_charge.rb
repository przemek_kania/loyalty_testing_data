module Accounts
  module Response
    class CanCharge < BaseResponse
      def initialize(api_charge)
        @code = :ok
        @data = api_charge.account.as_json
        @data[:errors] = []
        @message = "Account can be charged for #{api_charge.messages_cost}"
      end
    end
  end
end
