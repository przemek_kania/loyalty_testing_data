require 'spec_helper'

describe Accounts::ApiCharge do

  describe 'validates attributes' do
    it { is_expected.to validate_numericality_of(:message_price).only_integer.is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_numericality_of(:messages_quantity).only_integer.is_greater_than_or_equal_to(0) }
    it { is_expected.to validate_presence_of(:customer) }
  end

  let(:customer) { create(:customer) }
  let(:account) { subject.account }

  context 'with valid params' do
    before { expect(subject.valid?).to be true }
    let(:params) { {messages_quantity: 10, transaction_item_type: :push_message, customer: customer} }
    subject { described_class.new(params) }

    context '#balance_valid?' do
      it 'returns true if account has enough balance' do
        account.update_attributes(balance: 5000)
        expect(subject.balance_valid?).to eq true
      end

      it 'returns false if account does not have enough balance' do
        account.update_attributes(balance: 0)
        expect(subject.balance_valid?).to eq false
      end
    end

    context '#messages_cost' do
      let(:params) { {messages_quantity: '15', transaction_item_type: :push_message, customer: customer} }
      it 'returns valid cost for integers as strings' do
        expect(subject.messages_cost).to eq 150
      end
    end
  end

  context 'with invalid params' do
    before { expect(subject.valid?).to be false }

    it 'validates transaction type' do
      params = {messages_quantity: '10', transaction_item_type: :unknown, customer: customer}
      expect(subject.errors['transaction_item_type']).to include 'is not included in the list'
    end

    it 'returns all errors' do
      params = {}
      expect(subject.errors.messages).to eq({:messages_quantity => ["can't be blank", "is not a number"],
                                             :message_price => ["can't be blank", "is not a number"],
                                             :transaction_item_type => ["No price for ", "is not included in the list"],
                                             :customer => ["can't be blank"]})
    end
  end
end